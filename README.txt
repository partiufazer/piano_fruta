= Small piece of code to play sounds based on the number of the track
  Developed to play on a Raspberry Pi receiving keys from an external
  device as an Arduino or similar. To add more sounds, just put a wav
  file following the pattern NUMBER.wav.

= This following tutorial is used to guide you to make RpPi boots
  into this software directly, just changing some parameters.
  http://andrewdotni.ch/blog/2015/02/28/midi-synth-with-raspberry-p/
  
