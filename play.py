import pygame

pygame.init()
pygame.mixer.init()
pygame.mixer.music.set_volume(1)
clock = pygame.time.Clock()

sound_num = None
while True:
    try:
    	sound_num = str(input())
		
        pygame.mixer.music.load('sounds/' + sound_num[0] + '.wav')
        pygame.mixer.music.play()

    	while pygame.mixer.music.get_busy():
    	    clock.tick(30)
            	
    except SyntaxError:
	    print 'Invalid Number'

    except ValueError:
	    print 'Invalid Number'

    except pygame.error:
	    print 'Not found'
